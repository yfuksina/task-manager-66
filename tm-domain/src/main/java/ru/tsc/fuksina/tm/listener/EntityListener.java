package ru.tsc.fuksina.tm.listener;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.event.OperationEvent;
import ru.tsc.fuksina.tm.enumerated.EntityOperationType;

import javax.persistence.*;
import java.util.function.Consumer;

@NoArgsConstructor
public class EntityListener {

    @Nullable
    private static Consumer<OperationEvent> CONSUMER = null;

    @Nullable
    public static Consumer<OperationEvent> getConsumer() {
        return CONSUMER;
    }

    public static void setConsumer(@NotNull final Consumer<OperationEvent> consumer) {
        EntityListener.CONSUMER = consumer;
    }

    @PostLoad
    public void postLoad(@NotNull final Object entity) {
        sendMessage(entity, EntityOperationType.POST_LOAD);
    }

    @PrePersist
    public void prePersist(@NotNull final Object entity) {
        sendMessage(entity, EntityOperationType.PRE_PERSIST);
    }

    @PostPersist
    public void postPersist(@NotNull final Object entity) {
        sendMessage(entity, EntityOperationType.POST_PERSIST);
    }

    @PreRemove
    public void preRemove(@NotNull final Object entity) {
        sendMessage(entity, EntityOperationType.PRE_REMOVE);
    }

    @PostRemove
    public void postRemove(@NotNull final Object entity) {
        sendMessage(entity, EntityOperationType.POST_REMOVE);
    }

    @PreUpdate
    public void preUpdate(@NotNull final Object entity) {
        sendMessage(entity, EntityOperationType.PRE_UPDATE);
    }

    @PostUpdate
    public void postUpdate(@NotNull final Object entity) {
        sendMessage(entity, EntityOperationType.POST_UPDATE);
    }

    private void sendMessage(@NotNull final Object entity, @NotNull final EntityOperationType operationType) {
        if (CONSUMER == null) return;
        CONSUMER.accept(new OperationEvent(entity, operationType.toString()));
    }

}
