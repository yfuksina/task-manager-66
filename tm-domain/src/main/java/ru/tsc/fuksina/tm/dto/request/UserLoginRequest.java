package ru.tsc.fuksina.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class UserLoginRequest extends AbstractRequest {

    @NotNull
    private String login;

    @Nullable
    private String password;

    public UserLoginRequest(@NotNull final String login, @Nullable final String password) {
        this.login = login;
        this.password = password;
    }

}
