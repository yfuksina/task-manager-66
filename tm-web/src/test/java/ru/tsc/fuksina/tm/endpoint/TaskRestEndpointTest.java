package ru.tsc.fuksina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.fuksina.tm.client.TaskRestEndpointClient;
import ru.tsc.fuksina.tm.marker.IntegrationCategory;
import ru.tsc.fuksina.tm.model.Task;

import java.util.List;

@Category(IntegrationCategory.class)
public class TaskRestEndpointTest {

    @NotNull
    final TaskRestEndpointClient client = TaskRestEndpointClient.client();

    @NotNull
    final Task task1 = new Task("task 1");

    @NotNull
    final Task task2 = new Task("task 2");

    @NotNull
    final Task task3 = new Task("task 3");

    private static long INITIAL_SIZE;

    @Before
    public void init() {
        client.save(task1);
        client.save(task2);
        INITIAL_SIZE = client.count();
    }

    @After
    public void end() {
        client.clear();
    }

    @Test
    public void save() {
        @NotNull final String name = task3.getName();
        @NotNull final Task task = client.save(task3);
        Assert.assertNotNull(task);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(INITIAL_SIZE + 1, client.count());
    }

    @Test
    public void findAll() {
        @NotNull final List<Task> tasks = client.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INITIAL_SIZE, tasks.size());
    }

    @Test
    public void findById() {
        @NotNull final String name = task1.getName();
        @NotNull final Task task = client.findById(task1.getId());
        Assert.assertEquals(name, task.getName());
    }

    @Test
    public void existsById() {
        final boolean exists = client.existsById(task1.getId());
        final boolean notExists = client.existsById("123");
        Assert.assertTrue(exists);
        Assert.assertFalse(notExists);
    }

    @Test
    public void delete() {
        client.delete(task2);
        @NotNull final Task task = client.findById(task2.getId());
        Assert.assertNull(task);
    }

    @Test
    public void deleteAll() {
        client.clear(client.findAll());
        @NotNull final List<Task> tasks = client.findAll();
        Assert.assertEquals(0, tasks.size());
    }

    @Test
    public void clear() {
        client.clear();
        @NotNull final List<Task> tasks = client.findAll();
        Assert.assertEquals(0, tasks.size());
    }

    @Test
    public void count() {
        @NotNull final List<Task> tasks = client.findAll();
        Assert.assertEquals(tasks.size(), client.count());
    }

}
