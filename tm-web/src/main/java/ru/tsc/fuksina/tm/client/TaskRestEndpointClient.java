package ru.tsc.fuksina.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.tsc.fuksina.tm.model.Project;
import ru.tsc.fuksina.tm.model.Task;

import java.util.List;

public interface TaskRestEndpointClient {

    @NotNull
    String BASE_URL = "http://localhost:8080/api/tasks";

    static TaskRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskRestEndpointClient.class, BASE_URL);
    }

    @GetMapping("/findAll")
    List<Task> findAll();

    @GetMapping("/findById/{id}")
    Task findById(@PathVariable("id") String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @PostMapping("/save")
    Task save(@RequestBody Task task);

    @PostMapping("/delete")
    void delete(@RequestBody Task task);

    @PostMapping("/deleteAll")
    void clear(@RequestBody List<Task> tasks);

    @PostMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @DeleteMapping("/clear")
    void clear();

    @GetMapping("/count")
    long count();

}
