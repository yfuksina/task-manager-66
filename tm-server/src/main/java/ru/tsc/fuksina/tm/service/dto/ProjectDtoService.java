package ru.tsc.fuksina.tm.service.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.fuksina.tm.api.service.dto.IProjectDtoService;
import ru.tsc.fuksina.tm.dto.model.ProjectDto;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.exception.field.*;
import ru.tsc.fuksina.tm.repository.dto.ProjectDtoRepository;

import java.util.*;

@Getter
@Service
public class ProjectDtoService extends AbstractUserOwnedServiceDto<ProjectDto, ProjectDtoRepository> implements IProjectDtoService {

    @NotNull
    @Autowired
    private ProjectDtoRepository repository;

    @NotNull
    @Override
    @Transactional
    public ProjectDto create(@Nullable final String userId, @Nullable final String name) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        @NotNull final ProjectDto project = new ProjectDto();
        project.setUserId(userId);
        project.setName(name);
        @NotNull final ProjectDtoRepository repository = getRepository();
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).filter(item -> !item.isEmpty()).orElseThrow(DescriptionEmptyException::new);
        @NotNull final ProjectDto project = new ProjectDto();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        @NotNull final ProjectDtoRepository repository = getRepository();
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateStart,
            @Nullable final Date dateEnd
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).filter(item -> !item.isEmpty()).orElseThrow(DescriptionEmptyException::new);
        @NotNull final ProjectDto project = new ProjectDto();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(dateStart);
        project.setDateEnd(dateEnd);
        @NotNull final ProjectDtoRepository repository = getRepository();
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).filter(item -> !item.isEmpty()).orElseThrow(DescriptionEmptyException::new);
        @NotNull final ProjectDto project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        @NotNull final ProjectDtoRepository repository = getRepository();
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDto changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(status).orElseThrow(StatusEmptyException::new);
        @NotNull final ProjectDto project = findOneById(userId, id);
        project.setStatus(status);
        @NotNull final ProjectDtoRepository repository = getRepository();
        repository.save(project);
        return project;
    }

}
