package ru.tsc.fuksina.tm.repository.dto;

import org.springframework.stereotype.Repository;
import ru.tsc.fuksina.tm.dto.model.ProjectDto;

@Repository
public interface ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDto> {

}
