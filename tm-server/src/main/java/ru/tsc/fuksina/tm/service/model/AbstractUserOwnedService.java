package ru.tsc.fuksina.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.fuksina.tm.api.service.model.IService;
import ru.tsc.fuksina.tm.api.service.model.IUserOwnedService;
import ru.tsc.fuksina.tm.comparator.CreatedComparator;
import ru.tsc.fuksina.tm.comparator.DateStartComparator;
import ru.tsc.fuksina.tm.comparator.StatusComparator;
import ru.tsc.fuksina.tm.enumerated.Sort;
import ru.tsc.fuksina.tm.exception.field.IdEmptyException;
import ru.tsc.fuksina.tm.exception.field.UserIdEmptyException;
import ru.tsc.fuksina.tm.model.AbstractUserOwnedModel;
import ru.tsc.fuksina.tm.repository.model.AbstractUserOwnedRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends AbstractUserOwnedRepository<M>> extends AbstractService<M, R> implements IUserOwnedService<M>, IService<M> {

    @NotNull
    protected abstract AbstractUserOwnedRepository<M> getRepository();

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        if (sort == null) return findAll(userId);
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        return repository.findAllByUserId(userId, getSortType(sort.getComparator()));
    }

    @Nullable
    @Override
    public  M findOneById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @NotNull final M model) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        repository.deleteByUserIdAndId(userId, model.getId());
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        repository.deleteAllByUserId(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    public long getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final AbstractUserOwnedRepository<M> repository = getRepository();
        return repository.countByUserId(userId);
    }

    private String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else if (comparator == DateStartComparator.INSTANCE) return "status";
        else return "name";
    }

}
