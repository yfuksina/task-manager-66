package ru.tsc.fuksina.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.fuksina.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.fuksina.tm.api.service.dto.IProjectTaskDtoService;
import ru.tsc.fuksina.tm.dto.request.TaskBindToProjectRequest;
import ru.tsc.fuksina.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.tsc.fuksina.tm.dto.response.TaskBindToProjectResponse;
import ru.tsc.fuksina.tm.dto.response.TaskUnbindFromProjectResponse;
import ru.tsc.fuksina.tm.dto.model.SessionDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Getter
@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.fuksina.tm.api.endpoint.IProjectTaskEndpoint")
public final class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    @NotNull
    @Autowired
    private IProjectTaskDtoService projectTaskService;

    @NotNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        @Nullable final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse();
    }

}
