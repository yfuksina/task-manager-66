package ru.tsc.fuksina.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.model.AbstractModelDto;

import java.util.Collection;
import java.util.List;

public interface IServiceDto<M extends AbstractModelDto> {

    void add(@NotNull M model);

    void set(@NotNull Collection<M> models);

    void update(@NotNull M model);

    @NotNull
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull String id);

    void remove(@NotNull M model);

    void removeById(@NotNull String id);

    void clear();

    long getSize();

    boolean existsById(@NotNull String id);

}
